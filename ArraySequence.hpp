#ifndef ARRAY_SEQUENCE_H
#define ARRAY_SEQUENCE_H

#include "Sequence.hpp"
#include "DynamicArray.hpp"

template <typename T>
class ArraySequence : public Sequence<T>{

    private:
        DynamicArray<T> *_array;

    public:
        ArraySequence() : _array(new DynamicArray<T>) {}
        ArraySequence (T* data, int count);
        ArraySequence (const DynamicArray<T> &new_array);

        T& Get(int i) {return _array->operator[](i);}
        const T& Get(int i) const {return _array->operator[](i);}
        int getLength() const noexcept {return _array->getSize();}

        void Append(const T& value) {_array->set(0, value);}
        void Prepend(const T& value) {_array->set(getLength(), value);}
        void InsertAt(const T& value, int index) {_array->insert(index, value);}

        Sequence<T>* GetSubsequence(int startIndex, int endIndex) const;
        Sequence <T>* Concat(Sequence <T> *list) const;
        Sequence<T>* operator + (Sequence<T> *a) const {return Concat(a);}

        bool operator== (const ArraySequence<T> &list) const;
        bool operator!= (const ArraySequence<T> &list) const {return !(list == *this);}

        ~ArraySequence();

};

template<typename T>
ArraySequence<T>::ArraySequence(T* data, int count)
{
	_array = new DynamicArray<T>(data, count);
}

template<typename T>
ArraySequence<T>::ArraySequence(const DynamicArray<T> &new_array)
{
	_array = new DynamicArray<T>(new_array);
}


template<typename T>
Sequence<T>* ArraySequence<T>::GetSubsequence(int startIndex, int endIndex) const
{
	return new ArraySequence<T>(&_array->operator[](startIndex), (endIndex - startIndex));
}


template<typename T>
Sequence<T> *ArraySequence<T>::Concat(Sequence<T> *list) const
{
	int len = _array->getSize() + list->getLength();
	ArraySequence<T> *res = new ArraySequence<T>();
	res->_array->Resize(len);

	for (int i = 0; i < _array->getSize(); i++)
		res->Append(_array->operator[](i));

	for (int i = 0; i < list->getLength(); i++)
		res->Append(list->Get(i));
	return res;
}

template<typename T>
bool ArraySequence<T>::operator==(const ArraySequence<T> &list) const
{
	if (this->getLength() == 0 && list.getLength() == 0)
			return true;

	if (this->getLength() == list.getLength()) {
		for (int i = 0; i < list.getLength(); i++) {
			if (this->Get(i) == list.Get(i))
				return true;
		}
	}
	else return false;
}


template<typename T>
ArraySequence<T>::~ArraySequence()
{
	delete _array;
}

#endif // ARRAY_SEQUENCE_H
