cmake_minimum_required(VERSION 3.19)
project(LAB2)

enable_testing()

set(CMAKE_CXX_STANDARD 14)

include_directories(.)

add_subdirectory(TESTS)

add_executable(LAB2 main.cpp)
