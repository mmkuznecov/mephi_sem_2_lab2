#ifndef SQUARE_MATRIX_H
#define SQUARE_MATRIX_H

#include "DynamicArray.hpp"
#include "Complex.hpp"
#include <exception>
#include <cmath>
#include <typeinfo>
#include <type_traits>

using namespace std;


template <typename T>
class SquareMatrix{
    private:
        DynamicArray<DynamicArray<T>*>* matrix;
        int m_size;

    public:

        SquareMatrix();
        SquareMatrix(T** data, int matrix_size);
        SquareMatrix(SquareMatrix<T> *matrix_data);
        ~SquareMatrix();

        void Add(SquareMatrix<T> &matrix_data);
        void ScalarMult(int scalar);
        typename std::conditional<std::is_same<int, T>::value, float, T>::type Normalize();

        // Elementary manipulations

        void reshuflle_rows(int row1_index, int row2_index);
        void mult_row(int row_index, int k);
        void add_rows(int row1_index, int row2_index, int k);

        void reshuflle_columns(int column1_index, int column2_index);
        void mult_column(int column_index, int k);
        void add_columns(int column1_index, int column2_index, int k);



        void print_matrix();

        T Get(int i, int j);

};

template <typename T>
SquareMatrix<T>::SquareMatrix(){
    matrix = NULL;
    m_size = 0;
}

template <typename T>
SquareMatrix<T>::SquareMatrix(T** data, int matrix_size){

    matrix = new DynamicArray<DynamicArray<T>*>(matrix_size);

    for (int i = 0; i < matrix_size; i++){
        matrix->set(i, new DynamicArray<T>(data[i], matrix_size));
    }

    m_size = matrix_size;
}

template <typename T>
SquareMatrix<T>::SquareMatrix(SquareMatrix *matrix_data){

    this->m_size = matrix_data->m_size;

    this->matrix = new DynamicArray<DynamicArray<T>*>(m_size);

    for (int i = 0 ; i < m_size ; i++){
        matrix -> set(i, new DynamicArray<T>(*(matrix_data -> matrix -> get(i))));
    }
}

template <typename T>
SquareMatrix<T>::~SquareMatrix(){
    for (int i = 0; i < m_size; i++) {
        delete matrix -> get(i);
    }
}

template <typename T>
void SquareMatrix<T>::Add(SquareMatrix<T> &matrix_data){
    for (int i = 0; i < m_size; i++){
        for (int j = 0; j < m_size; j++) {
            T element = matrix_data.matrix -> get(i) -> get(j);
            matrix -> At(i) -> set(j, (matrix -> get(i) -> get(j)) + element);
        }
    }
}

template <typename T>
void SquareMatrix<T>::ScalarMult(int scalar){

    for (int i = 0; i < m_size ; i++){
        for (int j = 0; j < m_size; j++){
            matrix -> At(i) -> set(j, (matrix -> get(i) -> get(j)) * scalar);
        }
    }
}

template <typename T>
typename std::conditional<std::is_same<int, T>::value, float, T>::type SquareMatrix<T>::Normalize(){
    
    T sum = T();

    for (int i = 0; i < m_size; i++){
        for (int j = 0; j < m_size; j++){
            T el = matrix -> get(i) -> get(j);
            sum += el * el;
        }
    }


    return sqrt(sum);
}




template <typename T>
void SquareMatrix<T>::print_matrix(){

    for (int i = 0; i < m_size; i++){
        for (int j = 0; j < m_size; j++){
            cout << matrix -> get(i) -> get(j) <<  "\t";
        }

        cout << endl;
    }
}

template <typename T>
T SquareMatrix<T>::Get(int i, int j){
    return matrix -> get(i) -> get(j); 
}



/// OMGWTF
template <typename T>
void SquareMatrix<T>::reshuflle_rows(int row1_index, int row2_index){
    for (int i = 0; i < m_size; i++){
        T tmp_el = matrix -> At(row1_index) -> get(i);
        matrix -> At(row1_index) -> set(i, matrix -> At(row2_index) -> get(i));
        matrix -> At(row2_index) -> set(i, tmp_el);
    }
}

template <typename T>
void SquareMatrix<T>::mult_row(int row_index, int k){
    for (int i = 0; i < m_size; i++){
        matrix -> At(row_index) -> set(i, (matrix -> get(row_index) -> get(i)) * k);
    }
}

template <typename T>
void SquareMatrix<T>::add_rows(int row1_index, int row2_index, int k){
    for (int i = 0; i < m_size ; i++){
        matrix -> At(row1_index) -> set(i, (matrix -> At(row1_index) -> get(i)) + (matrix -> At(row2_index) -> get(i)*k));
    }
}

template <typename T>
void SquareMatrix<T>::reshuflle_columns(int column1_index, int column2_index){
    for (int i = 0; i < m_size; i++){
        T tmp_el = matrix -> At(i) -> get(column1_index);
        matrix -> At(i) -> set(column1_index, matrix -> At(i) -> get(column2_index));
        matrix -> At(i) -> set(column2_index, tmp_el);
    }
}

template <typename T>
void SquareMatrix<T>::mult_column(int column_index, int k){
    for (int i = 0; i < m_size; i++){
        matrix -> At(i) -> set(column_index, (matrix -> get(i) -> get(column_index)) * k);
    }
}

template <typename T>
void SquareMatrix<T>::add_columns(int column1_index, int column2_index, int k){
    for (int i = 0; i < m_size ; i++){
        matrix -> At(i) -> set(column1_index, (matrix -> At(i) -> get(column1_index)) + (matrix -> At(i) -> get(column2_index)*k));
    }
}

#endif // SQUARE_MATRIX_H

/*

 ^＿^
(｡･ω･｡)つ-☆・*。
⊂    |    ・゜+.
し ーＪ   °。+ *')
         .· ´  .·*')  .·')
          (¸.·´ (¸.·'* MakeLabaWork() Do you believe in Magic? ☆ﾞ
 */