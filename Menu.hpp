#ifndef MENU_H
#define MENU_H

#include <iostream>
#include <string>
#include "SquareMatrix.hpp"
#include "Complex.hpp"
#include <random>
#include <ctime>


const string options[] = {"0: Finish process", "1: Add two matrices", "2: Multiply matrix on scalar", "3: Normalize matrix", "4: Elementary manipulations"};
const string manip_type[] = {"1: Reshuffle", "2: Mult", "3: Add"};
const string matrix_type[] = {"1: Int", "2: Float", "3: Complex"};



int rand_gen(int a){

    return rand() % 10;
}

float rand_gen(float a){
    return static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/10));
}

Complex rand_gen(Complex a){
    float real = static_cast <float> (rand() % 10);
    float imag = static_cast <float> (rand() % 10);

    return Complex(real, imag);
}


int choice(){
    int ch;

    for (int i = 0; i < 5; i++){
        cout << options[i] << endl;
    }

    cin >> ch;
    return ch;
}

template <typename T>
T** get_matrix(int matrix_size){

    T** M__ = new T*[matrix_size];
    for(int i = 0; i < matrix_size; i++){
        M__[i] = new T[matrix_size];
    }

    for (int i = 0; i < matrix_size; i++){
        for (int j = 0; j < matrix_size; j++)
            M__[i][j] = rand_gen(T()); 
    
    }
    
    return M__;

}


template <typename T>
void Menu(int ch){


    cout << "Input matrix size" << endl;
    int matrix_size;
    cin >> matrix_size;

    switch(ch){
        case 1:{
            
            T** M1 = get_matrix<T>(matrix_size);
            T** M2 = get_matrix<T>(matrix_size);

            SquareMatrix<T> Matr1(M1, matrix_size);
            SquareMatrix<T> Matr2(M2, matrix_size);

            cout << "First Matrix" << endl;
            Matr1.print_matrix();

            cout << "Second Matrix" << endl;
            Matr2.print_matrix();

            Matr1.Add(Matr2);

            cout << "Matrices sum" << endl;
            Matr1.print_matrix();


            break; }
        case 2: {

            int k;

            cout << "Input scalar" << endl;
            cin >> k;

            T** M_sc = get_matrix<T>(matrix_size);

            SquareMatrix<T> Matr_sc(M_sc, matrix_size);

            cout << "Orig Matrix" << endl;

            Matr_sc.print_matrix();


            Matr_sc.ScalarMult(k);

            cout << "After scalar multiplication" << endl;

            Matr_sc.print_matrix();



            break; }

        case 3: {

            T** M_n = get_matrix<T>(matrix_size);

            SquareMatrix<T> Matr_n(M_n, matrix_size);

            cout << "Orig Matrix" << endl;

            Matr_n.print_matrix();

            cout << "Matrix Norm" << endl;
            cout << Matr_n.Normalize() << endl;

            break; }

        case 4: {

            T** M_man = get_matrix<T>(matrix_size);

            SquareMatrix<T> Matr_man(M_man, matrix_size);

            cout << "Orig Matrix" << endl;

            Matr_man.print_matrix();

            int oper_type;

            cout << "Choice type of elementary manipulation:" << endl;

            for (int i = 0; i < 3; i++){
                cout << manip_type[i] << endl;
            }

            cin >> oper_type;

            int dim_type;

            cout << "Rows or columns? 0: rows, 1: columns" << endl;

            cin >> dim_type;

            if (dim_type){

                switch (oper_type) {
                    case 1: {
                        int index1, index2;

                        cout << "Input indexes: ";

                        cin >> index1 >> index2;

                        Matr_man.reshuflle_columns(index1, index2);

                        break;
                    }

                    case 2:{
                        int index, k;

                        cout << "Input index: ";

                        cin >> index;

                        cout << "Input coefficient: ";

                        cin >> k;

                        Matr_man.mult_column(index, k);

                        break;
                    }

                    case 3:{
                        int index1, index2, k;

                        cout << "Input indexes: ";

                        cin >> index1 >> index2;

                        cout << "Input coefficient: ";

                        cin >> k;

                        Matr_man.add_columns(index1, index2, k);

                        break;
                    }
                }
            }

            else{

                switch (oper_type) {
                    case 1: {
                        int index1, index2;

                        cout << "Input indexes: ";

                        cin >> index1 >> index2;

                        Matr_man.reshuflle_rows(index1, index2);

                        break;
                    }

                    case 2:{
                        int index, k;

                        cout << "Input index: ";

                        cin >> index;

                        cout << "Input coefficient: ";

                        cin >> k;

                        Matr_man.mult_row(index, k);

                        break;
                    }

                    case 3:{
                        int index1, index2, k;

                        cout << "Input indexes: ";

                        cin >> index1 >> index2;

                        cout << "Input coefficient: ";

                        cin >> k;

                        Matr_man.add_rows(index1, index2, k);

                        break;
                    }
                }
            }

            cout << "Modified matrix" << endl;

            Matr_man.print_matrix();

        }
    }

}


void call_menu(){

    srand(time(0));

    int ch = choice();

    if(ch){
        
        cout << "Choose matrix type" << endl;
        for (int i = 0; i < 3; i++){
            cout << matrix_type[i] << endl;
        }

        int matrix_type;
        cin >> matrix_type;

        switch(matrix_type){
            case 1: {
                Menu<int>(ch);
                break; }
            case 2: {
                Menu<float>(ch);
                break; }
            case 3: {
                Menu<Complex>(ch);
                break; }
        }
    }

}

#endif // MENU_H