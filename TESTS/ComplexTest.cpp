#include "gtest/gtest.h"
#include "../Complex.hpp"

using namespace std;

TEST(Complex, Multiply) {
    Complex a = Complex(2.0, 0.0);
    Complex b = Complex(-2.0, 0.0);
    EXPECT_FLOAT_EQ((b * a).real, -4.0);
    EXPECT_FLOAT_EQ((b * a).im, 0.0);

    a = Complex(0.0, 2.0);
    b = Complex(0.0, 2.0);
    EXPECT_FLOAT_EQ((a * b).real, -4.0);
    EXPECT_FLOAT_EQ((a * b).im, 0.0);

    a = Complex(4.0, 2.0);
    b = Complex(5.0, -3.0);
    EXPECT_FLOAT_EQ((a * b).real, 26);
    EXPECT_FLOAT_EQ((a * b).im, -2);
    Complex c = Complex(b);
    EXPECT_FLOAT_EQ(c.real, b.real);
    EXPECT_FLOAT_EQ(c.im, b.im);
    EXPECT_TRUE(c == b);
}

TEST(Complex, Sum) {
    Complex a = Complex(4.0, 2.0);
    Complex b = Complex(5.0, -3.0);
    EXPECT_FLOAT_EQ((a + b).real, 9.0);
    EXPECT_FLOAT_EQ((a + b).im, -1.0);
}