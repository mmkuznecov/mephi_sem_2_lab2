#include "../LinkedList.hpp"
#include "gtest/gtest.h"

TEST(LinkedList, Index_vals){
    int *arr = new int[10];
    LinkedList<int> list(arr, 10);

    for (int i = 0; i < 10; i++){
        list[i] = i;
    }

    EXPECT_EQ(list[0], 0);
    EXPECT_EQ(list[3], 3);
    EXPECT_EQ(list[6], 6);
    EXPECT_EQ(list[9], 9);

    EXPECT_EQ(list.getLength(), 10);

}

TEST(LinkedList, InsertAt){
    int *arr = new int[10];
    LinkedList<int> list(arr, 10);

    for (int i = 0; i < 10; i++){
        list[i] = i;
    }

    list.InsertAt(1,1);

    EXPECT_EQ(1,list[1]);
    EXPECT_EQ(1, list[2]);
    EXPECT_EQ(2, list[3]);
}

TEST(LinkedList, FrontAndBack){
    int *arr = new int[10];
    LinkedList<int> list(arr, 10);

    for (int i = 0; i < 10; i++){
        list[i] = i;
    }

    list.Append(15);
    list.Prepend(19);

    EXPECT_EQ(19,list[-1]);
    EXPECT_EQ(15, list[0]);
}