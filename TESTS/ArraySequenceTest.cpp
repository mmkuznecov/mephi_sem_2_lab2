#include "gtest/gtest.h"
#include "../ArraySequence.hpp"

TEST(ArraySeq, Arr){
    int a[4] = {1,2,3,4};
    ArraySequence<int> arrseq = ArraySequence<int>(a, 4);

    EXPECT_EQ(arrseq.Get(0),1);
    EXPECT_EQ(arrseq.Get(1),2);
    EXPECT_EQ(arrseq.Get(3),4);


}

TEST(ArraySeq, Funcs){
    int a[4] = {1,2,3,4};
    ArraySequence<int> arrseq = ArraySequence<int>(a, 4);

    arrseq.InsertAt(1,10);

    EXPECT_EQ(arrseq.Get(0),1);
    EXPECT_EQ(arrseq.getLength(), 5);

}

TEST(ArraySeq, Concat){
    int a[4] = {1,2,3,4};
    ArraySequence<int> arrseq1 = ArraySequence<int>(a, 4);

    int b[4] = {5,6,7,8};
    ArraySequence<int> arrseq2 = ArraySequence<int>(b, 4);

    arrseq1.Concat(&arrseq2);

    EXPECT_EQ(arrseq1.getLength(), arrseq2.getLength());


}