#include "../DynamicArray.hpp"
#include "gtest/gtest.h"

TEST(DynamicArray, Indexation){
    int array_size = 10;
    DynamicArray<int> array(array_size);

    for (int i = 0; i < array_size; i ++){
        array[i] = i;
    }

    EXPECT_EQ(array[9], array[-1]);
    EXPECT_EQ(array[0], array[-10]);
    EXPECT_EQ(array[3], array[-7]);

}

TEST(DynamicArray, Values){
    int array_size = 10;
    DynamicArray<int> array(array_size);

    for (int i = 0; i < array_size; i ++){
        array[i] = i;
    }

    EXPECT_EQ(array[0], 0);
    EXPECT_EQ(array[3], 3);
    EXPECT_EQ(array[9], 9);

}

TEST(DynamicArray, Ins){
    int array_size = 10;

    DynamicArray<int> array(array_size);

    for (int i = 0; i < array_size; i ++){
        array[i] = i;
    }

    array.insert(2,20);

    EXPECT_EQ(array[2], 20);
    EXPECT_EQ(array.getSize(), array_size+1);

}

TEST(DynamicArray, Setter){
    int array_size = 10;

    DynamicArray<int> array(array_size);

    for (int i = 0; i < array_size; i ++){
        array[i] = i;
    }

    array.set(4, 100);
    EXPECT_EQ(array[4], 100);
    array.set(4, 99);
    EXPECT_EQ(array[4], 99);

    array.set(0, 12);
    array.set(0, 14);
    array.set(1, 40);

    EXPECT_EQ(array[0], 14);
    EXPECT_EQ(array[1], 40);

}