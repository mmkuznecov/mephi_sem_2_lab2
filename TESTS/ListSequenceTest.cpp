#include "gtest/gtest.h"
#include "../ListSequence.hpp"

TEST(ListSeq, Creation){
    int *arr = new int[10];
    LinkedList<int> list(arr, 10);

    for (int i = 0; i < 10; i++){
        list[i] = i;
    }

    ListSequence<int> ls = ListSequence<int>(arr, 10);

    EXPECT_EQ(ls.Get(2), 0);

}