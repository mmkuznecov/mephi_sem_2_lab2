#ifndef COMPLEX_H
#define COMPLEX_H

#include <iostream>
#include <cmath>

using namespace std;

class Complex {
public:
    float real, im;

    Complex() : real(), im() {}

    Complex(float real, float im) {
        this->real = real;
        this->im = im;
    }

    Complex(const Complex &ob) {
        real = ob.real;
        im = ob.im;
    };

    Complex operator+(float val) const {
        return Complex(this->real + val, this->im);
    }

    Complex operator-(float val) const {
        return Complex(this->real - val, this->im);
    }

    Complex operator+(const Complex& val) const {
        return Complex(this->real + val.real, this->im + val.im);
    }

    Complex operator-(const Complex& val) const {
        return Complex(this->real - val.real, this->im - val.im);
    }

    void operator+=(const Complex& val) {
        this->real += val.real;
        this->im += val.im;
    }

    Complex operator*(float val) const {
        return Complex(this->real * val, this->im * val);
    }

    Complex operator*(Complex val) const {
        return Complex(this->real * val.real - this->im * val.im, this->real * val.im + this->im * val.real);
    }

    bool operator==(const Complex& val) const {
        return this->real == val.real && this->im == val.im;
    }

    bool operator!=(const Complex& val) const {
        return !this->operator==(val);
    }

    friend ostream& operator<< (ostream &out, const Complex &complex_n){
        out << complex_n.real << " + (" << complex_n.im << ")i";   
    }

    void print(){
        cout << real << " + (" << im << ")i" << endl;
    }

    float length(){
        return std::sqrt(this->real * this->real + this->im * this->im);
    }

    int sgn(){
        if (this-> im < 0){ return -1;}
        else if (this-> im == 0){ return 0;}
        else {return -1;}
    }
        

};

// Custom sqrt() func overloading for 'Complex' class objects

namespace std{

Complex sqrt(Complex &complex_){
    float re = std::sqrt((complex_.real + complex_.length())/2);
    float imag = complex_.sgn() * std::sqrt((complex_.length() - complex_.real)/2);

    return Complex(re, imag);
}

}

#endif //COMPLEX_H
