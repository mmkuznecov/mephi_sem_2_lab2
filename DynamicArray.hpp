#ifndef DYN_ARRAY_H
#define DYN_ARRAY_H

#include <cassert>
#include <iostream>
#include <exception>

using namespace std;


template <typename T>
class DynamicArray{
    private:
        T *data;
        int size;

    public:

        // constructors
        
        DynamicArray (); // constructor by default
        DynamicArray (T *data_val, int count); // copy from array
        DynamicArray (DynamicArray<T> &a); // copy constructor
        DynamicArray (int size_val);

        ~DynamicArray(); // destructor

        // getters

        int getSize(){ return size; }

        T get(int index);
        T& At(int index);

        // setters

        void set(int index, T new_value);
        void insert(int index, T new_value);
        void Resize(int new_size);

        // external functions

        void prettyPrint();

        // operator overloading

        T& operator[](int index);
        //DynamicArray& operator= (const DynamicArray<T> &array_value);

};

template <typename T>
DynamicArray<T>::DynamicArray(){
    data = NULL;
    size = 0;
}

template <typename T>
DynamicArray<T>::DynamicArray (T *data_val, int count){
            
    assert (count >= 0);

    size = count;

    if (count){
        data = new T[count];
    }
    else{
        data = NULL;
    }

    for (int i = 0; i < count; i++){
        data[i] = data_val[i];
    }
}

template <typename T>
DynamicArray<T>::DynamicArray (DynamicArray<T> &a){
    int arg_size = a.size;

    assert (arg_size >= 0);

    size = arg_size;

    if(size){
        data = new T[size];
    }
    else{
        data = NULL;
    }

    for (int i = 0; i < size; i++){
        data[i] = a.data[i];
    }
}

template <typename T>
DynamicArray<T>::DynamicArray(int size_val){
    assert (size_val >= 0);

    size = size_val;
    if (size_val){
        data = new T[size_val];
    }
    else{
        data = NULL; // ???
    }
}

template <typename T>
DynamicArray<T>::~DynamicArray(){ 
    
    delete[] data;
}

template <typename T>
T DynamicArray<T>::get(int index){
    bool index_norm = index >= 0 && index < size;

    if (!index_norm){
        throw out_of_range("ERROR: INDEX OUT OF RANGE");
    }
    else{return data[index]; }
}

template <typename T>
T& DynamicArray<T>::At(int index){
    bool index_norm = index >= 0 && index < size;

    if (!index_norm){
        throw out_of_range("ERROR: INDEX OUT OF RANGE");
    }
    else{return data[index]; }
}

template <typename T>
void DynamicArray<T>::set(int index, T new_value){
    bool index_norm = index >= 0 && index < size;

    if(!index_norm){
        throw out_of_range("ERROR: INDEX OUT OF RANGE");
    }
    else{data[index] = new_value;}
}

template <typename T>
void DynamicArray<T>::insert(int index, T new_value) {
    T* new_data = new T[size+1];

    for (int i = 0; i < index - 1; i++){
        new_data[i] = data[i];
    }

    new_data[index] = new_value;

    for (int i = index+1; i < size+1; i++){
        new_data[i] = data[i-1];
    }

    size += 1;

    delete[] data;
    data = new_data;
}

template <typename T>
void DynamicArray<T>::Resize(int new_size){

    assert (new_size >= 0);

    if (new_size != size){
        T* new_data = new T[new_size];
        
 
        for (int i = 0; i < size; i ++){
            new_data[i] = data[i];
        }
        
        delete[] data;
        data = new_data;

        size = new_size;
    }

}


template <typename T>
void DynamicArray<T>::prettyPrint(){
    cout << "[";
    for (int i = 0; i < size-1; i ++){
        cout << data[i] << ",";
    }
    cout << data[size-1];
    cout << "]" << endl;
}

template <typename T>
T& DynamicArray<T>::operator[](int index){
    assert (index >= size * (-1) && index < size);

    if (index >= 0){
        return data[index];
    }
    if (index < 0){
        return data[size+index];
    }

}

#endif